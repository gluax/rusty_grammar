# rusty_grammar &emsp; [![Latest Version]][crates.io] [![API]][documentation] [![rusty_grammar: rustc 1.46.0+]][Rust 1.46.0]

[Latest Version]: https://img.shields.io/crates/v/rusty_grammar.svg
[crates.io]: https://crates.io/crates/rusty_grammar
[API]: https://docs.rs/rusty_grammar/badge.svg
[documentation]: https://docs.rs/rusty_grammar
[rusty_grammar: rustc 1.46.0+]: https://img.shields.io/badge/rusty_grammar-rustc_1.46.0+-lightgray.svg
[Rust 1.46.0]: https://blog.rust-lang.org/2020/05/07/Rust.1.46.0.html

`rusty_grammar` is a library that makes use of a modified CYK algorithm
to define grammars and understand language.
	
## Example
```rust
struct G {}

impl<'grammar> Grammar<'grammar> for G {
	fn convert(&self) -> Vec<GrammarRule<'grammar>> {
		let mut rules = Vec::new();
		rules.push(GrammarRule{ left_symbol: "ActionSentence", right_symbol: "Verb NounClause | Verb NounClause PrepClause" });
		rules.push(GrammarRule{ left_symbol: "NounClause", right_symbol: "Count ANoun | Adjective Noun" });
		rules.push(GrammarRule{ left_symbol: "PrepClause", right_symbol: "Prep NounClause" });
		rules.push(GrammarRule{ left_symbol: "ANoun", right_symbol: "Adjective Noun" });
		rules.push(GrammarRule{ left_symbol: "Adjective", right_symbol: "adjective" });
		rules.push(GrammarRule{ left_symbol: "Prep", right_symbol: "prep" });
		rules.push(GrammarRule{ left_symbol: "Verb", right_symbol: "verb" });
		rules.push(GrammarRule{ left_symbol: "Noun", right_symbol: "noun" });
		rules.push(GrammarRule{ left_symbol: "Count", right_symbol: "definiteArticle | indefiniteArticle | number" });
		rules
	}
}

struct WB {}

impl WordBank for WB {
	fn lookup(&self, word: &str) -> &str {
		match word {
			"examine" => "verb",
			"sword" => "noun",
			"rusty" => "adjective",
			_ => "dne"
		}
	}
}

fn main() {
	let g = G{};
	let wb = WB{};
	let input = "examine rusty sword";
	let cyk: CYK<WB> = CYK::new(g, wb);
	let res = cyk.memoized_parse(input);
	println!("{}", res);
	println!("final_res: {:?}", res.get_final());
}
```
